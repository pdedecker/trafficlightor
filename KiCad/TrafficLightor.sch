EESchema Schematic File Version 4
LIBS:TrafficLightor-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR0101
U 1 1 5D572626
P 3350 3750
F 0 "#PWR0101" H 3350 3500 50  0001 C CNN
F 1 "GND" H 3355 3577 50  0000 C CNN
F 2 "" H 3350 3750 50  0001 C CNN
F 3 "" H 3350 3750 50  0001 C CNN
	1    3350 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5D575651
P 1700 1650
F 0 "C1" H 1815 1696 50  0000 L CNN
F 1 "0.33 uF" H 1815 1605 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 1738 1500 50  0001 C CNN
F 3 "~" H 1700 1650 50  0001 C CNN
	1    1700 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5D575CA0
P 2600 1650
F 0 "C2" H 2715 1696 50  0000 L CNN
F 1 "0.1 uF" H 2715 1605 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 2638 1500 50  0001 C CNN
F 3 "~" H 2600 1650 50  0001 C CNN
	1    2600 1650
	1    0    0    -1  
$EndComp
$Comp
L MCU_Microchip_ATtiny:ATtiny24A-PU U1
U 1 1 5D5719E9
P 3350 2750
F 0 "U1" H 2821 2796 50  0000 R CNN
F 1 "ATtiny24A-PU" H 2821 2705 50  0000 R CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 3350 2750 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc8183.pdf" H 3350 2750 50  0001 C CNN
	1    3350 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 1800 2150 1800
Wire Wire Line
	2150 1800 2150 1900
Connection ~ 2150 1800
Wire Wire Line
	2150 1800 2600 1800
$Comp
L power:GND #PWR0102
U 1 1 5D578F86
P 2150 1900
F 0 "#PWR0102" H 2150 1650 50  0001 C CNN
F 1 "GND" H 2155 1727 50  0000 C CNN
F 2 "" H 2150 1900 50  0001 C CNN
F 3 "" H 2150 1900 50  0001 C CNN
	1    2150 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 1500 2150 1800
$Comp
L Regulator_Linear:L7805 U2
U 1 1 5D574D5B
P 2150 1200
F 0 "U2" H 2150 1442 50  0000 C CNN
F 1 "L7805" H 2150 1351 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2175 1050 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 2150 1150 50  0001 C CNN
	1    2150 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 1200 2600 1200
Wire Wire Line
	1700 1500 1700 1200
Wire Wire Line
	1700 1200 1850 1200
Wire Wire Line
	2600 1500 2600 1200
Connection ~ 2600 1200
Wire Wire Line
	2600 1200 3350 1200
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5D57BEDA
P 950 1300
F 0 "J1" H 868 975 50  0000 C CNN
F 1 "Power" H 868 1066 50  0000 C CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 950 1300 50  0001 C CNN
F 3 "~" H 950 1300 50  0001 C CNN
	1    950  1300
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5D57C98F
P 1150 1400
F 0 "#PWR0103" H 1150 1150 50  0001 C CNN
F 1 "GND" H 1155 1227 50  0000 C CNN
F 2 "" H 1150 1400 50  0001 C CNN
F 3 "" H 1150 1400 50  0001 C CNN
	1    1150 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 1400 1150 1300
Text Label 4650 2900 2    50   ~ 0
MOSI
Text Label 4650 2800 2    50   ~ 0
MISO
Text Label 4650 2600 2    50   ~ 0
RESET
Text Label 3350 1850 0    50   ~ 0
VCC
Wire Wire Line
	3350 3650 3350 3750
Text Label 3350 3750 0    50   ~ 0
GND
Text Label 2150 1900 0    50   ~ 0
GND
Text Label 1150 1400 0    50   ~ 0
GND
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5D5871A2
P 5550 3200
F 0 "J4" H 5630 3192 50  0000 L CNN
F 1 "Pushbutton_Top" H 5630 3101 50  0000 L CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 5550 3200 50  0001 C CNN
F 3 "~" H 5550 3200 50  0001 C CNN
	1    5550 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5D5882CF
P 5350 3300
F 0 "#PWR0104" H 5350 3050 50  0001 C CNN
F 1 "GND" H 5355 3127 50  0000 C CNN
F 2 "" H 5350 3300 50  0001 C CNN
F 3 "" H 5350 3300 50  0001 C CNN
	1    5350 3300
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5D58ACC3
P 5250 3550
F 0 "#PWR0105" H 5250 3300 50  0001 C CNN
F 1 "GND" H 5255 3377 50  0000 C CNN
F 2 "" H 5250 3550 50  0001 C CNN
F 3 "" H 5250 3550 50  0001 C CNN
	1    5250 3550
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J6
U 1 1 5D593D14
P 6450 4700
F 0 "J6" H 6530 4742 50  0000 L CNN
F 1 "PedConn_1" H 6530 4651 50  0000 L CNN
F 2 "Connector_JST:JST_PH_S3B-PH-K_1x03_P2.00mm_Horizontal" H 6450 4700 50  0001 C CNN
F 3 "~" H 6450 4700 50  0001 C CNN
	1    6450 4700
	1    0    0    1   
$EndComp
Text Label 3950 2150 0    50   ~ 0
PA0
Text Label 3950 2250 0    50   ~ 0
PA1
Text Label 6250 4600 2    50   ~ 0
Ped_R
Text Label 6250 4700 2    50   ~ 0
Ped_G
$Comp
L Connector_Generic:Conn_01x03 J5
U 1 1 5D596F68
P 6450 4150
F 0 "J5" H 6530 4192 50  0000 L CNN
F 1 "PedConn_2" H 6530 4101 50  0000 L CNN
F 2 "Connector_JST:JST_PH_S3B-PH-K_1x03_P2.00mm_Horizontal" H 6450 4150 50  0001 C CNN
F 3 "~" H 6450 4150 50  0001 C CNN
	1    6450 4150
	1    0    0    1   
$EndComp
Text Label 6250 4150 2    50   ~ 0
Ped_G
Text Label 6250 4050 2    50   ~ 0
Ped_R
$Comp
L Device:R R_PedR1
U 1 1 5D59C438
P 5200 4500
F 0 "R_PedR1" V 4993 4500 50  0000 C CNN
F 1 "330" V 5084 4500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5130 4500 50  0001 C CNN
F 3 "~" H 5200 4500 50  0001 C CNN
	1    5200 4500
	0    -1   1    0   
$EndComp
$Comp
L Device:R R_PedG1
U 1 1 5D59CEEC
P 5200 4700
F 0 "R_PedG1" V 5407 4700 50  0000 C CNN
F 1 "1000" V 5316 4700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5130 4700 50  0001 C CNN
F 3 "~" H 5200 4700 50  0001 C CNN
	1    5200 4700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5350 4700 5600 4700
Wire Wire Line
	5600 4700 5600 4150
Wire Wire Line
	5600 4150 6250 4150
Connection ~ 5600 4700
Wire Wire Line
	5600 4700 6250 4700
Wire Wire Line
	5350 4500 5500 4500
Wire Wire Line
	5800 4500 5800 4600
Wire Wire Line
	5800 4600 6250 4600
Wire Wire Line
	5500 4500 5500 4050
Wire Wire Line
	5500 4050 6250 4050
Connection ~ 5500 4500
Wire Wire Line
	5500 4500 5800 4500
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5D58A339
P 5550 3550
F 0 "J3" H 5630 3542 50  0000 L CNN
F 1 "Modeswitch_Bottom" H 5630 3451 50  0000 L CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 5550 3550 50  0001 C CNN
F 3 "~" H 5550 3550 50  0001 C CNN
	1    5550 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3550 5350 3550
Wire Wire Line
	3950 2150 4600 2150
Wire Wire Line
	4600 2150 4600 1950
Wire Wire Line
	4600 1950 5200 1950
Wire Wire Line
	3950 2250 4750 2250
Wire Wire Line
	4750 2250 4750 2100
Wire Wire Line
	4750 2100 5200 2100
Wire Wire Line
	5200 2350 5200 2250
$Comp
L Device:R R_CarR1
U 1 1 5D5B1DFF
P 5350 1950
F 0 "R_CarR1" V 5143 1950 50  0000 C CNN
F 1 "330" V 5234 1950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5280 1950 50  0001 C CNN
F 3 "~" H 5350 1950 50  0001 C CNN
	1    5350 1950
	0    -1   1    0   
$EndComp
$Comp
L Device:R R_CarO1
U 1 1 5D5B240A
P 5350 2100
F 0 "R_CarO1" V 5143 2100 50  0000 C CNN
F 1 "330" V 5234 2100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5280 2100 50  0001 C CNN
F 3 "~" H 5350 2100 50  0001 C CNN
	1    5350 2100
	0    -1   1    0   
$EndComp
$Comp
L Device:R R_CarG1
U 1 1 5D5B26B9
P 5350 2250
F 0 "R_CarG1" V 5143 2250 50  0000 C CNN
F 1 "510" V 5234 2250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5280 2250 50  0001 C CNN
F 3 "~" H 5350 2250 50  0001 C CNN
	1    5350 2250
	0    -1   1    0   
$EndComp
Wire Wire Line
	5800 1950 5800 1800
Wire Wire Line
	5800 1800 6450 1800
Wire Wire Line
	5500 2100 5650 2100
Wire Wire Line
	5900 2100 5900 1900
Wire Wire Line
	5900 1900 6450 1900
Wire Wire Line
	5500 2250 5550 2250
Wire Wire Line
	6050 2250 6050 2000
Wire Wire Line
	6050 2000 6450 2000
Wire Wire Line
	5500 1950 5750 1950
Wire Wire Line
	5550 2250 5550 2750
Wire Wire Line
	5550 2750 6450 2750
Connection ~ 5550 2250
Wire Wire Line
	5550 2250 6050 2250
Wire Wire Line
	5650 2100 5650 2650
Wire Wire Line
	5650 2650 6450 2650
Connection ~ 5650 2100
Wire Wire Line
	5650 2100 5900 2100
Wire Wire Line
	5750 1950 5750 2550
Wire Wire Line
	5750 2550 6450 2550
Connection ~ 5750 1950
Wire Wire Line
	5750 1950 5800 1950
Text Label 6450 1800 2    50   ~ 0
Car_R
Text Label 6450 1900 2    50   ~ 0
Car_O
Text Label 6450 2000 2    50   ~ 0
Car_G
Text Label 6450 2550 2    50   ~ 0
Car_R
Text Label 6450 2650 2    50   ~ 0
Car_O
Text Label 6450 2750 2    50   ~ 0
Car_G
Wire Wire Line
	5050 4500 4200 4500
Wire Wire Line
	5050 4700 4100 4700
Wire Wire Line
	4100 3250 3950 3250
Wire Wire Line
	4100 3250 4100 4700
Wire Wire Line
	3950 3050 4700 3050
Wire Wire Line
	3950 3150 4600 3150
$Comp
L power:GND #PWR0110
U 1 1 5D5E4A24
P 4500 2500
F 0 "#PWR0110" H 4500 2250 50  0001 C CNN
F 1 "GND" H 4505 2327 50  0000 C CNN
F 2 "" H 4500 2500 50  0001 C CNN
F 3 "" H 4500 2500 50  0001 C CNN
	1    4500 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 2500 4650 2500
Text Label 4650 2500 2    50   ~ 0
GND
Text Label 3950 3050 0    50   ~ 0
PB0
Text Label 3950 3150 0    50   ~ 0
PB1
Text Label 3950 3250 0    50   ~ 0
PB2
Text Label 3950 3350 0    50   ~ 0
RESET
Text Label 3950 2850 0    50   ~ 0
PA7
Text Label 3950 2750 0    50   ~ 0
PA6
Text Label 3950 2650 0    50   ~ 0
PA5
Text Label 3950 2550 0    50   ~ 0
PA4
Text Label 3950 2450 0    50   ~ 0
PA3
Text Label 3950 2350 0    50   ~ 0
PA2
Wire Wire Line
	4200 2450 4200 4500
Wire Wire Line
	3950 2450 4200 2450
Wire Wire Line
	3950 2850 4100 2850
NoConn ~ 4100 2850
Wire Wire Line
	4600 3650 4600 3150
Wire Wire Line
	4600 3650 5350 3650
Connection ~ 1700 1200
Wire Wire Line
	1150 1200 1400 1200
$Comp
L Device:D_Schottky D1
U 1 1 5D5A4B80
P 1550 1200
F 0 "D1" H 1550 984 50  0000 C CNN
F 1 "D_Schottky" H 1550 1075 50  0000 C CNN
F 2 "Diode_THT:D_T-1_P5.08mm_Horizontal" H 1550 1200 50  0001 C CNN
F 3 "~" H 1550 1200 50  0001 C CNN
	1    1550 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	3950 3350 4450 3350
Wire Wire Line
	3950 2550 4400 2550
Wire Wire Line
	3950 2750 4300 2750
Wire Wire Line
	3950 2650 4350 2650
$Comp
L Connector:Conn_01x06_Male J9
U 1 1 5D59318C
P 4850 2700
F 0 "J9" H 4822 2582 50  0000 R CNN
F 1 "Programmer" H 4822 2673 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 4850 2700 50  0001 C CNN
F 3 "~" H 4850 2700 50  0001 C CNN
	1    4850 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	3950 2350 5200 2350
Text Label 4650 2700 2    50   ~ 0
SCK
Wire Wire Line
	4300 2750 4300 2900
Wire Wire Line
	4350 2650 4350 2800
Wire Wire Line
	4500 2400 4650 2400
Connection ~ 3350 1850
Text Label 4650 2400 2    50   ~ 0
VCC_e
Wire Wire Line
	4300 2900 4650 2900
Wire Wire Line
	4350 2800 4650 2800
Wire Wire Line
	4450 3350 4450 2600
Wire Wire Line
	4450 2600 4650 2600
Wire Wire Line
	4400 2700 4650 2700
Wire Wire Line
	4400 2550 4400 2700
$Comp
L Device:D_Schottky D2
U 1 1 5D5D6B42
P 4300 1850
F 0 "D2" H 4300 1634 50  0000 C CNN
F 1 "D_Schottky" H 4300 1725 50  0000 C CNN
F 2 "Diode_THT:D_T-1_P5.08mm_Horizontal" H 4300 1850 50  0001 C CNN
F 3 "~" H 4300 1850 50  0001 C CNN
	1    4300 1850
	1    0    0    1   
$EndComp
$Comp
L Device:R RST_PU1
U 1 1 5D6BE627
P 4250 3550
F 0 "RST_PU1" V 4043 3550 50  0000 C CNN
F 1 "4700" V 4134 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4180 3550 50  0001 C CNN
F 3 "~" H 4250 3550 50  0001 C CNN
	1    4250 3550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4450 1850 4500 1850
Wire Wire Line
	4500 2400 4500 1850
Wire Wire Line
	3350 1850 4150 1850
Wire Wire Line
	4150 1850 4150 2000
Wire Wire Line
	4150 2000 4250 2000
Wire Wire Line
	4250 2000 4250 3400
Connection ~ 4150 1850
Wire Wire Line
	4250 3700 3950 3700
Wire Wire Line
	3950 3700 3950 3350
Connection ~ 3950 3350
$Comp
L Connector_Generic:Conn_01x04 J8
U 1 1 5D5A9C7B
P 6650 2750
F 0 "J8" H 6730 2742 50  0000 L CNN
F 1 "CarConn_2" H 6730 2651 50  0000 L CNN
F 2 "Connector_JST:JST_PH_S4B-PH-K_1x04_P2.00mm_Horizontal" H 6650 2750 50  0001 C CNN
F 3 "~" H 6650 2750 50  0001 C CNN
	1    6650 2750
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J7
U 1 1 5D5A91F2
P 6650 2000
F 0 "J7" H 6730 1992 50  0000 L CNN
F 1 "CarConn_1" H 6730 1901 50  0000 L CNN
F 2 "Connector_JST:JST_PH_S4B-PH-K_1x04_P2.00mm_Horizontal" H 6650 2000 50  0001 C CNN
F 3 "~" H 6650 2000 50  0001 C CNN
	1    6650 2000
	1    0    0    1   
$EndComp
Wire Wire Line
	4700 3050 4700 3200
Wire Wire Line
	4700 3200 5350 3200
Wire Wire Line
	3350 1200 3350 1850
Text Label 6250 4250 2    50   ~ 0
VCC
Text Label 6250 4800 2    50   ~ 0
VCC
Text Label 6450 2850 2    50   ~ 0
VCC
Text Label 6450 2100 2    50   ~ 0
VCC
$EndSCHEMATC

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <util/delay.h>

const int kCarGPin_PortA = PA2;
const int kCarOPin_PortA = PA1;
const int kCarRPin_PortA = PA0;
const int kPedGPin_PortB = PB2;
const int kPedRPin_PortA = PA3;

typedef enum State {
    StatePreCarGreen = 0
  , StateCarGreen
  , StateCarOrange
  , StatePrePedGreen
  , StateCarRed
} State;

const int kNStates = 5;

volatile State gCurrentState = StateCarRed;

const int kNOverFlowsCarGreen = 22;
const int kNOverFlowsCarOrange = 4;
const int kNOverFlowsCarRed = 22;
const int kNOverFlowsPreGreen = 4;

volatile int gNOverflowsRemaining = 0;

void SetEnablePCINT8Interrupt(uint8_t enable) {
    if (enable) {
        PCMSK1 |= (1 << PCINT8);    // enable pin change interrupt for PCINT8 (button top)
        GIMSK |= (1 << PCIE1);          // enable pin change interrupt for PCINT[8:11]
    } else {
        PCMSK1 &= ~(1 << PCINT8);
    }
}

void SetEnableTimer1(uint8_t enable) {
    cli();
    if (enable) {
        TCNT1 = 0;
        TIMSK1 |= (1 << TOIE1);
        TIFR1 |= (1 << TOV0);
    } else {
        TIMSK1 &= ~(1 << TOIE1);
        TIFR1 |= (1 << TOV0);
    }
    sei();
}

void InitChip() {
    // set main clock prescaler to 1 / 8.
    CLKPR = (1 << CLKPCE);
    CLKPR = 0b00000011;

    // disable ADC.
    ADCSRA &= ~(1 << ADEN);
    PRR |= (1 << PRADC);

    // disable serial I/O
    PRR |= (1 << PRUSI);

    // disable timer 1
    //PRR |= (1 << PRTIM1);

    // set I/O pins to input.
    DDRA = 0;
    DDRB = DDRB & 0xF0;

    // set 8-bit timer clock prescaler to 1 / 1024, no output compare
    // or other functionality.
    TCCR0A = 0;
    TCCR0B = 0b00000101;
    // enable timer 0 overflow interrupt.
    TIMSK0 = (1 << TOIE0);

    TCCR1B |= 0b010;    // timer 1 clock prescaler 1/8

}

void InitPins() {
    // set LED pins to output
    DDRA |=  (1 << kCarGPin_PortA);
    DDRA |=  (1 << kCarOPin_PortA);
    DDRA |=  (1 << kCarRPin_PortA);
    DDRB |=  (1 << kPedGPin_PortB);
    DDRA  |=  (1 << kPedRPin_PortA);

    // activate pull-up resistor on PB0
    DDRB &= ~(1 << PB0);
    PORTB |= (1 << PB0);
}

void DisableLEDs() {
    PORTA |=  (1 << kCarGPin_PortA);
    PORTA |=   (1 << kCarOPin_PortA);
    PORTA |=  (1 << kCarRPin_PortA);
    PORTB |=  (1 << kPedGPin_PortB);
    PORTA  |=  (1 << kPedRPin_PortA);
}

void IncrementState() {
    gCurrentState = ((gCurrentState + 1) % kNStates);
    TCNT0 = 0;
    TIFR0 |= (1 << TOV0);

    DisableLEDs();

    switch (gCurrentState) {
	case StatePreCarGreen:
	    PORTA = PORTA & ~(1 << kCarRPin_PortA);
	    PORTA = PORTA & ~(1 << kPedRPin_PortA);
	    gNOverflowsRemaining = kNOverFlowsPreGreen;
	    break;
	case StateCarGreen:
	    PORTA = PORTA & ~(1 << kCarGPin_PortA);
	    PORTA = PORTA & ~(1 << kPedRPin_PortA);
	    gNOverflowsRemaining = kNOverFlowsCarGreen;
	    break;
	case StateCarOrange:
	    PORTA = PORTA & ~(1 << kCarOPin_PortA);
	    PORTA = PORTA & ~(1 << kPedRPin_PortA);
	    gNOverflowsRemaining = kNOverFlowsCarOrange;
	    break;
	case StatePrePedGreen:
	    PORTA = PORTA & ~(1 << kCarRPin_PortA);
	    PORTA = PORTA & ~(1 << kPedRPin_PortA);
	    gNOverflowsRemaining = kNOverFlowsPreGreen;
	    break;
	case StateCarRed:
	    PORTA = PORTA & ~(1 << kCarRPin_PortA);
	    PORTB = PORTB & ~(1 << kPedGPin_PortB);
	    gNOverflowsRemaining = kNOverFlowsCarRed;
	    break;
    }
}



ISR(TIM0_OVF_vect) {
    if (gNOverflowsRemaining > 0) {
	       gNOverflowsRemaining -= 1;
    } else {
	       IncrementState();
    }
}

ISR(PCINT1_vect) {
    // top button has changed
    uint8_t isHigh =  PINB & (1 << PB0);
    if (!isHigh) {
        gCurrentState = StatePreCarGreen;
        IncrementState();
    }
    SetEnablePCINT8Interrupt(0);    // take care of bouncing
    SetEnableTimer1(1);
}

ISR(TIM1_OVF_vect) {
    SetEnablePCINT8Interrupt(1);    // assume that we're done bouncing
    SetEnableTimer1(0);
}

int main() {

    InitChip();
    InitPins();

    SetEnablePCINT8Interrupt(1);
    IncrementState();
    sei();

    for ( ; ; ) {
	       sleep_mode();
    }

    return 0;
}
